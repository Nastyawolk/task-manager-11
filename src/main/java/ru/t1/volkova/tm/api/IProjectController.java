package ru.t1.volkova.tm.api;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void removeProjectById();

    void removeProjectByIndex();

    void showProjectById();

    void showProjectByIndex();

    void showProjects();

    void updateProjectById();

    void updateProjectByIndex();

}

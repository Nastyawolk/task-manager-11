package ru.t1.volkova.tm.api;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void removeTaskById();

    void removeTaskByIndex();

    void showTaskById();

    void showTaskByIndex();

    void showTasks();

    void updateTaskById();

    void updateTaskByIndex();

}
